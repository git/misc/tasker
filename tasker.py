# tasker.py -- Manage a to-do list via the web.
#
# Copyright 2003, 2004, 2008, 2017 Russ Allbery <eagle@eyrie.org>
#
# Permission is hereby granted, free of charge, to any person obtaining a copy
# of this software and associated documentation files (the "Software"), to
# deal in the Software without restriction, including without limitation the
# rights to use, copy, modify, merge, publish, distribute, sublicense, and/or
# sell copies of the Software, and to permit persons to whom the Software is
# furnished to do so, subject to the following conditions:
#
# The above copyright notice and this permission notice shall be included in
# all copies or substantial portions of the Software.
# 
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.  IN NO EVENT SHALL THE
# AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
# LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
# FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS
# IN THE SOFTWARE.

"""Manage a to-do list via the web.

This module implements a CGI application which maintains a to-do list.  It
isn't particularly suitable for use as a regular package or as part of a
Python script.  Instead, it is expected to be invoked with::

    import tasker
    tasker.cgimain()

The simplest way to install this program is to create a two-line Python script
containing the above, put it in the same directory as a tasker_config.py
configuration file as described below, and configure your web server to run
that script using whatever URL you prefer.  Note that multiple separate
instances of Tasker can be supported on the same system by creating separate
wrapper files and tasker_config.py files for each one.

A file tasker_config.py is expected to be found on the load path and is used
for configuration data.  It must set the following variables:

root
    The path to the to-do database, which must be readable and writable by
    whatever user your CGI scripts run as.

archive
    The path to the to-do archive, where completed tasks are stored just in
    case.  Note that they aren't currently stored with any meaningful file
    names.

style_url
    The URL to the style sheet used to format all of the pages returned by the
    to-do manager.  It may be the style sheet which comes with the program or
    some other style sheet you've customized.

shortcuts
    The only complicated variable set to anything other than a string.  This
    should be set to a list of shortcuts that you want to see in the menu
    bar.  Each shortcut should be a tuple of three elements.  The first is a
    string used as the link that appears in the menu bar, the second is the
    priority limit, and the third is the group limit.  The priority limit may
    be 0 to indicate no limit, and the group limit may be '' (the empty
    string) to similarly indicate no group limit.

    Example::

        shortcuts = [('All', 0, ''), ('Urgent', 1, ''), ('Work', 0, 'Work')]

    This sets up three shortcuts, one that shows all open tasks, one that
    shows only open tasks with priority 1, and one that shows only open tasks
    in the Work group.

This module may eventually be expanded to include other functionality besides
the CGI interface, including functions for use in other Python scripts, but as
yet those plans are tentative.
"""

# Load system modules that we use.
import cgi
import cgitb; cgitb.enable()
import os
import re
import sys
import textwrap
import time
import urllib

# Use the right locale so that alphabetic characters are properly recognized.
import locale
locale.setlocale(locale.LC_ALL, '')

# Load our configuration.  This is normally tasker_config.py in the same
# directory as the small wrapper CGI script and contains configuration for
# that particular instance of the todo manager.
import tasker_config as config

# This page header is common to all generated pages.  Variables are in all
# caps surrounded by %%, and are substituted when the header is printed.
HTML_PAGE_HEADER = '''\
Content-Type: text/html

<?xml version="1.0" encoding="iso-8859-1"?>
<!DOCTYPE html
    PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN"
    "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">

<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" lang="en">
<head>
  <title>Tasker</title>
  <link rel="stylesheet" href="%STYLE%" type="text/css" />
  <meta http-equiv="content-type" content="text/html; charset=iso-8859-1" />
</head>

<body>

<div class="header">
  <h1>Tasker</h1>

  <form action="%URL%" method="get">
    <table class="header">
      <tr>
        <td class="shortcut">
%SHORTCUTS%
        </td>
        <td class="limit">
          <label for="limit">Limit:</label>
          <select name="limit" id="limit">
%LIMITS%
          </select>
          <label for="glimit">Group:</label>
          <select name="glimit" id="glimit">
            <option value="">-- Any --</option>
%GROUPS%
          </select>
          <input type="submit" value="Index" />
        </td>
      </tr>
    </table>
  </form>
</div>
'''

# The form at the bottom of the main index page for entering a new task.
# Variables are in all caps surrounded by %%, and are substituted when the
# header is printed.
HTML_TASK_FORM = '''\
<div class="form">
  <h2>New Task</h2>

  <form action="%URL%" method="post">
    <table class="form">
      <tr>
        <th class="label"><label for="priority">Priority:</label></th>
        <td>
          <select name="priority" id="priority">
            <option value="1">1</option>
            <option value="2">2</option>
            <option value="3" selected="selected">3</option>
            <option value="4">4</option>
            <option value="5">5</option>
          </select>
        </td>
      </tr>
      <tr>
        <th class="label"><label for="group">Group:</label></th>
        <td>
          <select name="group" id="group">
%GROUPS%
          </select>
        </td>
      </tr>
      <tr>
        <th class="label"><label for="category">Category:</label></th>
        <td><input type="text" size="25" name="category" id="category" /></td>
      </tr>
      <tr>
        <th class="label"><label for="task">Task:</label></th>
        <td><input type="text" size="80" name="task" id="task" /></td>
      </tr>
      <tr>
        <th class="label"><label for="subtask">Subtasks:</label></th>
        <td>
          <input type="text" size="80" name="subtask-1" id="subtask" /><br />
          <input type="text" size="80" name="subtask-2" id="subtask" /><br />
          <input type="text" size="80" name="subtask-3" id="subtask" /><br />
          <input type="text" size="80" name="subtask-4" id="subtask" /><br />
          <input type="text" size="80" name="subtask-5" id="subtask" />
        </td>
      </tr>
      <tr>
        <td></td>
        <td class="submit">
          <input type="submit" name="action" value="Add Task" />
          <input type="reset" value="Reset" />
        </td>
      </tr>
    </table>
    <input type="hidden" name="limit" value="%LIMIT%" />
    <input type="hidden" name="glimit" value="%GLIMIT%" />
    <input type="hidden" name="subcount" value="5" />
  </form>
</div>

<div class="form">
  <h2>New Group</h2>

  <form action="%URL%" method="post">
    <table class="form">
      <tr>
        <th class="label"><label for="group">Group:</label></th>
        <td><input type="text" size="25" name="group" id="group" /></td>
        <td class="submit">
          <input type="submit" name="action" value="Add Group" />
          <input type="reset" value="Reset" />
        </td>
      </tr>
    </table>
  </form>
</div>
'''

# The form used to edit the detail of an existing task.  Variables are in
# all caps surrounded by %%, and are substituted when the header is
# printed.
HTML_DETAIL_FORM = '''\
<div class="edit">
  <h2>Edit Task</h2>

  <form action="%URL%/%NAME%" method="post">
    <table class="form">
      <tr>
        <th class="label"><label for="priority">Priority:</label></th>
        <td>
          <select name="priority" id="priority">
%PRIORITIES%
          </select>
        </td>
      </tr>
      <tr>
        <th class="label"><label for="task">Task:</label></th>
        <td>
          <input value="%TASK%" type="text" size="80" name="task" id="task" />
        </td>
      </tr>
      <tr>
        <th class="label"><label for="category">Category:</label></th>
        <td>
          <input value="%CATEGORY%" type="text" size="25" name="category"
                 id="category" />
        </td>
      </tr>
      <tr>
        <th class="label"><label for="subtask">Subtasks:</label></th>
        <td>
%SUBTASKS%
        </td>
      </tr>
      <tr>
        <td></td>
        <td class="submit">
          <input type="submit" name="action" value="Update" />
          <input type="reset" value="Reset" />
        </td>
      </tr>
    </table>
    <input type="hidden" name="limit" value="%LIMIT%" />
    <input type="hidden" name="glimit" value="%GLIMIT%" />
    <input type="hidden" name="date" value="%DATE%" />
%DONE%
    <input type="hidden" name="subcount" value="%SUBCOUNT%" />
    <input type="hidden" name="donecount" value="%DONECOUNT%" />
  </form>
</div>
'''

# Used to report parsing errors in the task files.
class FormatError(StandardError):
    pass

# Used to report errors in the submitted form data.
class CGIError(StandardError):
    pass

def substitute(template, vars):
    """Substitute vars into template.

    For each variable in the dictionary vars, substitutes all occurrances
    of the string formed by surrounding that variable with % signs with the
    value of that variable.  The variable URL is always substituted with the
    virtual path to the script (SCRIPT_NAME), and the variable URLBASE with
    the containing directory of the virtual path to the script.
    """
    for key, value in vars.items():
        template = template.replace('%' + key + '%', value)
    if os.environ.has_key('SCRIPT_NAME'):
        url = os.environ['SCRIPT_NAME']
        urlbase = os.path.dirname(url)
        template = template.replace('%URL%', url)
        template = template.replace('%URLBASE%', urlbase)
    return template

def group_menu(selected = None):
    groups = os.listdir(config.root)
    groups.sort()
    options = ''
    for group in groups:
        options += '            '
        options += '<option value="' + cgi.escape(group, quote = True) + '"'
        if group == selected:
            options += ' selected="selected"'
        options += '>' + cgi.escape(group) + '</option>\n'
    return options.rstrip()

def priority_menu(selected = 5):
    options = ''
    for priority in ('1', '2', '3', '4', '5'):
        options += '            '
        options += '<option value="' + priority + '"'
        if int(priority) == selected:
            options += ' selected="selected"'
        options += '>' + priority + '</option>\n'
    return options.rstrip()

def shortcuts_menu():
    html = ''
    for shortcut in config.shortcuts:
        (name, limit, glimit) = shortcut
        if os.environ.has_key('SCRIPT_NAME'):
            url = os.environ['SCRIPT_NAME']
        else:
            url = '/cgi-bin/tasker'
        if limit > 0:
            url += '?limit=' + str(limit)
        if glimit != '':
            if limit > 0:
                url += '&'
            else:
                url += '?'
            url += 'glimit=' + urllib.quote(glimit)
        link = '<a href="' + url + '">' + cgi.escape(name) + '</a>'
        if html != '':
            html += ' &bull;\n'
        html += '          ' + link
    return html

def limit_options(limit, glimit):
    options  = '      <input type="hidden" name="limit" value="'
    options +=   str(limit) + '" />\n'
    options += '      <input type="hidden" name="glimit" value="'
    options +=   cgi.escape(glimit, quote = True) + '" />\n'
    return options

class Subtask:
    def __init__(self, string = None, type = None):
        if string:
            if type:
                self.type = type
                self.desc = string
            else:
                self.type = string[0]
                self.desc = string[1:].lstrip()

    def form(self, name, hidden = False):
        output  = '<input type="'
        if hidden:
            output += 'hidden'
        else:
            output += 'text'
        output += '" size="80" name="' + cgi.escape(name, quote = True) + '"'
        output += ' value="' + cgi.escape(self.desc, quote = True) + '" />'
        output += '<br />\n'
        return output

    def write(self, fh):
        line = textwrap.fill(self.desc, 74, initial_indent = self.type + ' ',
                             subsequent_indent = '  ',
                             break_on_hyphens = False)
        fh.write(line + "\n")

    def html(self, path, index, limit = 5, glimit = ""):
        if os.environ.has_key('SCRIPT_NAME'):
            url = os.environ['SCRIPT_NAME']
        else:
            url = '/cgi-bin/tasker'
        path = urllib.quote(path)
        query = '?limit=' + str(limit)
        if glimit:
            query += '&glimit=' + urllib.quote(glimit)
        output  = '<tr>\n'
        output += '  <td class="action">\n'
        output += '    <form action="' + url + '/' + path + '"'
        output +=      ' method="post">\n'
        output += '      <input type="hidden" name="subdid" value="'
        output +=      str(index) + '" />\n'
        output += limit_options(limit, glimit)
        output += '      <input type="submit" value="Done" />\n'
        output += '    </form>\n'
        output += '  </td>\n'
        output += '  <td>' + cgi.escape(self.desc) + '</td>\n'
        output += '</tr>\n'
        return output

class Task:
    def __init__(self, group, file = None):
        self.group = group
        if file:
            self.load(file)

    def load_header(self, fh):
        last = None
        headers = {}
        for line in fh:
            line = line.rstrip()
            if line == "":
                break
            elif line[0] == " " or line[0] == "\t":
                if not last:
                    raise FormatError, \
                          "continuation line without preceding header"
                headers[last] += line[1:]
            else:
                match = re.match(r'([^\s:]+):\s*(.*)', line)
                if not match:
                    raise FormatError, "header expected"
                headers[match.group(1)] = match.group(2)
                last = match.group(1)
        self.task = headers['Task']
        self.priority = headers['Priority']
        self.date = headers['Date']
        if headers.has_key('Category'):
            self.category = headers['Category']
        else:
            self.category = None

    def load_subtasks(self, fh):
        working = None
        subtasks = []
        for line in fh:
            line = line.rstrip()
            if line == "":
                continue
            elif line[0] == " ":
                if not working:
                    raise FormatError, \
                          "continuation line without preceding subtask"
                working += ' ' + line.lstrip()
            else:
                if working:
                    subtasks.append(Subtask(working))
                working = line
        if working:
            subtasks.append(Subtask(working))
        self.done = []
        self.subtasks = []
        for subtask in subtasks:
            if subtask.type == 'x':
                self.done.append(subtask)
            else:
                self.subtasks.append(subtask)

    def load(self, file):
        self.file = file
        self.name = os.path.basename(file)
        if not self.group:
            self.group = os.path.dirname(file)
        fh = open(os.path.join(config.root, file), "r")
        self.load_header(fh)
        self.load_subtasks(fh)

    def fillout(self, form, file = None):
        if file:
            self.file = file
            self.name = os.path.basename(file)
        else:
            group = form.getfirst('group')
            dir = os.path.join(config.root, group)
            if not os.path.isdir(dir) or dir == config.root:
                raise CGIError, "invalid group " + form.getfirst('group')
            files = [int(x) for x in os.listdir(dir)]
            if files:
                self.name = str(max(files) + 1)
            else:
                self.name = str(1)
            self.file = os.path.join(group, self.name)
        self.task = form.getfirst('task')
        if self.task is None or self.task.isspace():
            raise CGIError, "empty task name"
        self.priority = form.getfirst('priority')
        if form.has_key('date'):
            self.date = form.getfirst('date')
        else:
            self.date = time.strftime('%Y-%m-%d %H:%M:%S')
        if form.has_key('category'):
            self.category = form.getfirst('category')
        else:
            self.category = None
        self.subtasks = []
        self.done = []
        subcount = int(form.getfirst('subcount'))
        for i in range(1, subcount + 1):
            if form.has_key('subtask-' + str(i)):
                subtask = Subtask(form.getfirst('subtask-' + str(i)), '-')
                self.subtasks.append(subtask)
        if form.has_key('donecount'):
            donecount = int(form.getfirst('donecount'))
            if donecount > 0:
                for i in range(1, donecount + 1):
                    subtask = Subtask(form.getfirst('done-' + str(i)), 'x')
                    self.done.append(subtask)

    def save(self, file = None):
        if not file:
            file = self.file
        fh = open(os.path.join(config.root, file), "w")
        wrapper = textwrap.TextWrapper(width = 74, subsequent_indent = '  ')
        fh.write(wrapper.fill('Task: ' + self.task) + "\n")
        fh.write(wrapper.fill('Date: ' + self.date) + "\n")
        fh.write(wrapper.fill('Priority: ' + self.priority) + "\n")
        if self.category:
            fh.write(wrapper.fill('Category: ' + self.category) + "\n")
        fh.write("\n")
        for subtask in self.done:
            subtask.write(fh)
        for subtask in self.subtasks:
            subtask.write(fh)
        fh.close()

    def finish_subtask(self, index):
        subtask = self.subtasks.pop(index)
        subtask.type = 'x'
        date = time.strftime('%Y-%m-%d %H:%M:%S')
        subtask.desc = subtask.desc + ' (done ' + date + ')'
        self.done.append(subtask)
        self.save()

    def html_detail(self, header, value):
        output  = '  <tr>\n'
        output += '    <th class="detail">' + header + ':</th>\n'
        output += '    <td class="detail">' + cgi.escape(value) + '</td>\n'
        output += '  </tr>\n'
        return output

    def form(self, limit = 5, glimit = ""):
        priorities = priority_menu(int(self.priority))
        if self.category:
            category = cgi.escape(self.category, quote = True)
        else:
            category = ""
        subtasks = ''
        subcount = 0
        for subtask in self.subtasks:
            subcount += 1
            subtasks += '          '
            subtasks += subtask.form('subtask-' + str(subcount))
        sublast = subcount + 5
        while subcount < sublast:
            subcount += 1
            subtasks += '          '
            subtasks += '<input type="text" size="80"'
            subtasks += ' name="subtask-' + str(subcount) + '" /><br />\n'
        subtasks = subtasks.rstrip()
        done = ''
        donecount = 0
        for subtask in self.done:
            donecount += 1
            done += '    '
            done += subtask.form('done-' + str(donecount), hidden = True)
        done = done.rstrip()
        variables = {'PRIORITIES': priorities,
                     'TASK': cgi.escape(self.task, quote = True),
                     'SUBTASKS': subtasks, 'DONE': done,
                     'SUBCOUNT': str(subcount), 'DONECOUNT': str(donecount),
                     'DATE': cgi.escape(self.date, quote = True),
                     'NAME': urllib.quote(cgi.escape(self.file, quote = True)),
                     'LIMIT': str(limit), 'CATEGORY': category,
                     'GLIMIT': urllib.quote(cgi.escape(glimit, quote = True))}
        return substitute(HTML_DETAIL_FORM, variables)

    def html(self, limit = 5, glimit = ""):
        output  = '<h2 class="detail">Task Detail</h2>\n\n'
        output += '<table class="detail">\n'
        output += self.html_detail('Task', self.task)
        if self.category:
            output += self.html_detail('Category', self.category)
        output += self.html_detail('Date', self.date)
        output += self.html_detail('Priority', self.priority)
        output += '</table>\n\n'
        name = cgi.escape(self.file, quote = True)
        if self.subtasks:
            output += '<p>Open subtasks:</p>\n\n'
            output += '<table class="subtask">\n'
            subcount = 0
            for subtask in self.subtasks:
                subcount += 1
                output += subtask.html(name, subcount, limit, glimit)
            output += '</table>\n\n'
        if self.done:
            output += '<p>Completed subtasks:</p>\n\n'
            output += '<table class="subtask">\n'
            for subtask in self.done:
                output += '  <tr>\n'
                output += '    <td class="action"></td>\n'
                output += '    <td class="task">'
                output += cgi.escape(subtask.desc) + '</td>\n'
                output += '  </tr>\n'
            output += '</table>\n\n'
        output += self.form(limit, glimit)
        return output

    def html_short(self, limit = 5, glimit = ""):
        if os.environ.has_key('SCRIPT_NAME'):
            url = os.environ['SCRIPT_NAME']
        else:
            url = '/cgi-bin/tasker'
        path = urllib.quote(self.group) + '/' + urllib.quote(self.name)
        query = '?limit=' + str(limit)
        if glimit:
            query += '&glimit=' + urllib.quote(glimit)
        if self.category:
            category = cgi.escape(self.category)
        else:
            category = ""
        output  = '<tr>\n'
        output += '  <td class="action">\n'
        output += '    <form action="' + url + query + '"'
        output +=      ' method="post">\n'
        output += '      <input type="hidden" name="did" value="'
        output +=      cgi.escape(self.group + '/' + self.name, quote = True)
        output +=      '" />\n'
        output += limit_options(limit, glimit)
        output += '      <input type="submit" value="Done" />\n'
        output += '    </form>\n'
        output += '  </td>\n'
        output += '  <td class="category">' + category + '</td>\n'
        output += '  <td class="task"><a href="' + url + '/' + path
        output +=      query + '">' + cgi.escape(self.task) + '</a></td>\n'
        output += '  <td class="priority">'
        output +=      cgi.escape(self.priority) + '</td>\n'
        output += '  <td class="date">'
        output +=      cgi.escape(self.date.split()[0]) + '</td>\n'
        output += '</tr>\n'
        return output

class Group:
    def __init__(self, name = None):
        if name:
            self.load(name)

    def load(self, name):
        self.name = name
        self.tasks = []
        path = os.path.join(config.root, name)
        files = os.listdir(path)
        files.sort()
        for file in files:
            if not file.isdigit():
                continue
            self.tasks.append(Task(name, os.path.join(name, file)))
        tasks = [(x.priority, x.category, x.name, x) for x in self.tasks]
        tasks.sort()
        self.tasks = [x for (priority, category, name, x) in tasks]

    def save(self):
        for task in self.tasks:
            task.save()

    def html(self, limit = 5, glimit = ''):
        if glimit and self.name != glimit:
            return ''
        output = ''
        for task in self.tasks:
            if int(task.priority) > limit:
                continue
            output += task.html_short(limit, glimit)
        if output:
            header  = '<div class="group">\n'
            header += '<h2>' + cgi.escape(self.name) + '</h2>\n'
            header += '<table rules="cols" cellpadding="5%">\n'
            header += '<tr>\n'
            header += '  <th class="action"></th>\n'
            header += '  <th class="category">Category</th>\n'
            header += '  <th class="task">Task</th>\n'
            header += '  <th class="priority">Pri</th>\n'
            header += '  <th class="date">Date</th>\n'
            header += '</tr>\n'
            output  = header + output + '</table>\n</div>\n'
        return output

def print_header(limit = 5, glimit = None):
    variables = {'STYLE': config.style_url,
                 'GROUPS': group_menu(glimit),
                 'LIMITS': priority_menu(limit),
                 'SHORTCUTS': shortcuts_menu()}
    print substitute(HTML_PAGE_HEADER, variables)

def parse_limits(form):
    limit = 5
    glimit = ""
    if form and form.has_key('limit'):
        limit = int(form.getfirst('limit'))
    if form and form.has_key('glimit'):
        glimit = form.getfirst('glimit')
    return (limit, glimit)

def index(form = None):
    (limit, glimit) = parse_limits(form)
    print_header(limit, glimit)
    groups = os.listdir(config.root)
    groups.sort()
    for group in groups:
        if os.path.isdir(os.path.join(config.root, group)):
            print Group(group).html(limit, glimit)
    options = group_menu(glimit)
    print substitute(HTML_TASK_FORM, {'GROUPS': options, 'LIMIT': str(limit),
                                      'GLIMIT': glimit})
    print '</body>\n</html>'

def show_task(path, form):
    (limit, glimit) = parse_limits(form)
    print_header(limit, glimit)
    print Task(None, path).html(limit, glimit)
    print '</body>\n</html>'

def add_task(form):
    group = form.getfirst('group')
    if group is None:
        raise CGIError, "empty group name"
    if not group.isalnum():
        raise CGIError, "invalid group name " + group
    if not os.path.isdir(os.path.join(config.root, group)):
        raise CGIError, "invalid group name " + group
    task = Task(group)
    task.fillout(form)
    task.save()
    index(form)

def add_group(form):
    group = form.getfirst('group')
    if group is None:
        raise CGIError, "empty group name"
    if not group.isalnum():
        raise CGIError, "invalid group name " + group
    path = os.path.join(config.root, group)
    if os.path.isdir(path):
        raise CGIError, "group name " + group + " already exists"
    os.mkdir(path)
    index(form)

def edit_task(path, form):
    task = Task(None)
    task.fillout(form, path)
    task.save()
    show_task(path, form)

def finish_task(form):
    file = form.getfirst('did')
    (group, number) = file.split('/')
    if file.find('/') < 0 or not group.isalnum() or not number.isdigit():
        raise CGIError, "invalid task " + file
    patharchive = os.path.join(config.archive, group)
    old = os.path.join(config.root, file)
    timestamp = time.strftime('%Y%m%dT%H%M%S')
    id = timestamp + '.' + str(os.getpid())
    if not os.path.isdir(patharchive):
        os.mkdir(patharchive)
    new = os.path.join(patharchive, id)
    os.rename(old, new)
    index(form)

def finish_subtask(path, form):
    task = Task(None, path)
    task.finish_subtask(int(form.getfirst('subdid')) - 1)
    show_task(path, form)

def cgimain():
    try:
        method = os.environ['REQUEST_METHOD']
    except KeyError:
        method = 'GET'
    try:
        path = os.environ['PATH_INFO']
        path = urllib.unquote(path)
        (group, number) = path[1:].split('/')
        if path.find('/') < 0 or not group.isalnum() or not number.isdigit():
            raise CGIError, "invalid task " + path
    except KeyError:
        path = ""
    if method == 'POST':
        form = cgi.FieldStorage()
        if path and form.has_key('subdid'):
            finish_subtask(path[1:], form)
        elif path:
            edit_task(path[1:], form)
        elif form.has_key('action'):
            action = form.getfirst('action')
            if action == 'Add Task':
                add_task(form)
            elif action == 'Add Group':
                add_group(form)
            else:
                raise CGIError, "unknown action " + action
        elif form.has_key('did'):
            finish_task(form)
        else:
            raise CGIError, "unknown action"
    else:
        if os.environ.has_key('QUERY_STRING'):
            form = cgi.FieldStorage()
        else:
            form = None
        if path:
            show_task(path[1:], form)
        else:
            index(form)

if __name__ == "__main__":
    cgimain()
    sys.exit()
