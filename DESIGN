                          Tasker Design Overview

GOALS

  The purpose of this work is to create a to-do list management system
  that I'll actually use and keep up-to-date.  It has the following
  requirements:

   * Web accessible interface suitable for use as my home page, so that
     I'll always see the current items and can make quick changes and/or
     mark items as done.

   * Simple file-based database structure so that I can easily edit items
     using a text editor as well as creating them through a web interface.

   * The ability to pick a random item off the to-do list and mail it to
     me on a regular basis, as a prompt to keep making progress through
     the list.

   * A system suitable for handling a task breakdown with sufficient
     granularity that each separate task entry is small enough to be
     easily tackled by itself.

   * An ability to prune the tasks in various ways, such as only high
     priority tasks, and show random selections of lower-priority tasks so
     that I don't forget about them but am not overwhelmed by all of them
     at once.

  So far, presenting a current to-do list on the web in the form of
  multiple tasks organized by group has been finished.  Non-web access to
  the database, including mailing, and random display of low-priority
  tasks has not yet been done.

FORMAT

  The database is organized as follows:

      Group #1
          Task #1
          Task #2
           - Subtask #1
           - Subtask #2
           - Subtask #3
          Task #3
      Group #2
          Task #1

  and so forth.  Physically, this is represented as a directory containing
  one directory for each task group.  Each task is a single file in the
  appropriate directory.

  A task has the format:

      Header: Value
      Header: Value
      [...]

      - Subtask #1
      - Subtask #2

  It is similar to a mail message in that it starts with a header composed
  of key/value pairs using the same syntax as the header of a mail message
  (including continuation lines).  The header is separated from the body
  by a blank line.  The body is a list of subtasks in the form of a
  bulletted list.  Each line of the body must either begin with a bullet
  (starting a subtask), begin with whitespace (a continuation of the
  previous line), or be empty.

  The recognized headers to start are Task, which contains the short
  description of the task, Date, which contains the date the entry was
  created (in ISO format, optionally with a space and then the time),
  Priority, which contains a number from 1 to 5 with 1 indicating the
  highest priority, and Category, which contains a subcategory used just
  sorting tasks within a group.

  Subtasks normally begin with -.  Alternately, they may begin with x,
  which indicates a task already complete.  Further bullets may be defined
  in the future.

CGI PROTOCOL

  The primary interface is implemented as a single CGI application.  All
  HTML is generated internally by that application, although it does use a
  single external style sheet that must be available to the application
  while running (unless you want really ugly output).  The HTML is
  intended to be XHTML 1.0 Strict, meeting the requirements for HTML 4.0
  compliance so that it can be served as text/html.  Semantic markup is
  used where possible, with layout and formatting done using CSS.  The
  output is designed for standard compliant browsers and tested in
  Mozilla; some additional work may be required to accomodate other
  browsers.

  Requests are distinguished based on protocol.  All requests that change
  data (such as submissions of new tasks or marking tasks or subtasks
  done) is done via the POST method (in accordance with the HTTP
  protocol).  Requests to display data are done via the GET method.  If
  there is any path information (extra path data in the URL after the name
  of the CGI script), that information is taken to identify a particular
  task and operations are performed on that task.  Otherwise, the
  top-level overview is displayed and operations are assumed to be
  top-level operations (like marking whole tasks complete or adding new
  tasks).

  For GET requests, the URL is the URL of the CGI script, optionally
  followed by the path to a particular task (in the form <group>/<task>
  where <task> is the sequence number of the task in that group and is
  assigned sequentially and potentially reused), and then optionally
  followed by a limit parameter and a glimit parameter.  limit will limit
  display of top-level tasks to ones with at least that priority, and
  glimit will limit display to only tasks in that group.  Since limit and
  glimit are intended to be persistant in a session unless the user
  changes them again, they're preserved in the URL even when looking at
  particular tasks (where they aren't meaningful).

  For POST requests, the action is determined based on the data in the
  POST.  If the parameter subdid is set, and there is path information in
  the URL, that particular subtask of the task identified by the path
  information is marked as done.  If the parameter did is set, that task
  is marked as done.  Otherwise, the parameter action is expected to be
  set to either "Add Task" to create a new task or "Add Group" to create a
  new group.  Again, limit and glimit are expected to be persistant, so
  they must be included in any POST form submission as hidden fields.
