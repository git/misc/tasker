                       User-Visible Tasker Changes

Tasker 0.4 (2017-12-27)

    Better verify group and task names.  Reject empty or whitespace-only
    tasks, allow group names containing numbers, and improve error
    reporting for invalid tasks or groups.  Patch from Julien ÉLIE.

    When archiving a task, keep the group in the archive path.  Patch from
    Julien ÉLIE.

    Do not try to parse files whose names are not numbers, which avoids
    problems with editor backup files or other files left in the database
    directory.  Patch from Julien ÉLIE.

    Allow non-ASCII characters in group names.  Patch from Julien ÉLIE.

    Label reset buttons and properly escape some of the URL construction.
    Patch from Julien ÉLIE.

    Disable word-wrapping on hyphens because undoing the wrapping later
    results in spurious whitespace.  Thanks, Julien ÉLIE.

Tasker 0.3 (2008-07-05)

    Add the date of completion to finished subtasks.

    Use ISO timestamps for the file names of closed tasks instead of
    seconds since epoch to make it easier for humans to tell when tasks
    were closed.

Tasker 0.2 (2004-02-15)

    Number the first post to an empty group correctly so that starting
    a new group will work.

Tasker 0.1 (2004-01-02)

    Initial public release.  Contains only the CGI interface in a single
    Python module, with a sample wrapper and config script and manual
    installation instructions.
