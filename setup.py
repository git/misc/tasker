#!/usr/bin/env python
#
# setup.py -- Installation script for tasker.
#
# Copyright 2004 Russ Allbery <eagle@eyrie.org>
# See README for complete license information.

# Keep the wrapping of the description to 65 columns and make sure there is no
# trailing newline so that PKG-INFO looks right.
"""Tasker: CGI-based to-do manager

Tasker is a to-do manager that tracks tasks in user-defined
groups, assigning each a priority and optional subtasks, and
presenting a simple web-based interface for marking them as done
and seeing particular subsets of the list."""

import sys
import tasker
from distutils.core import setup

def die(message):
    """Report a fatal error message and then exit."""
    sys.stderr.write(message + "\n")
    sys.exit(1)

# I'm using all sorts of random stuff from Python 2.3 and don't feel like
# finding backward-compatible alternatives.  From past experience, by the time
# that this is stable enough for other people to really want to use it, this
# will be a trivial dependency to satisfy.
if sys.version_info < (2, 3):
    die("This is Python %d.%d.  tasker requires at least Python 2.3."
        % sys.versioninfo[:2])

# The metadata for setup.
doclines = __doc__.split("\n")
classifiers = """\
Development Status :: 3 - Alpha
Intended Audience :: End Users/Desktop
Intended Audience :: System Administrators
Environment :: Web Environment
License :: OSI Approved :: MIT License
Operating System :: OS Independent
Programming Language :: Python
Topic :: Internet :: WWW/HTTP :: Dynamic Content
Topic :: Office/Business :: News/Diary
"""
metadata = {
    'name':             'tasker',
    'version':          '0.4',
    'author':           'Russ Allbery',
    'author_email':     'eagle@eyrie.org',
    'url':              'http://www.eyrie.org/~eagle/software/tasker/',
    'description':      doclines[0],
    'long_description': "\n".join(doclines[2:]),
    'license':          'MIT',
    'classifiers':      filter(None, classifiers.split("\n")),
    'platforms':        'any',
    'keywords':         ['web', 'cgi', 'todo'],

    'py_modules':       ['tasker'],
    'data_files':       [('share/tasker',
                          ['tasker', 'tasker.css', 'tasker_config.py'])]
}

# Actually invoke the setup routine.
setup(**metadata)
