# tasker_config.py -- Configuration information for Tasker.
#
# All of these configuration variables must be set.  Putting this file in the
# same directory as the CGI script wrapper will generally be sufficient for
# Python to find it, since the web server generally sets the working directory
# to the directory containing the CGI script and Python adds the current
# directory to the load path.
#
# If your web server does *not* do this, you may need to add something like:
#
#     import sys
#     sys.path.append('/home/USER/cgi-bin')
#
# to the wrapper script to point it to the directory that contains this
# configuration file.

# The path to the to-do data.  Task groups will be subdirectories of this
# directory.  Normally relative paths will end up being relative to the
# directory containing the CGI script, but this may depend on your web server.
root = './data'

# The path to the archive area for completed tasks.  Tasks marked completed
# will be moved here rather than deleted just in case.  See above for relative
# path handling.
archive = './archive'

# The URL to the style sheet that you want to use for formatting.  This will
# depend on how you've configured your web server and where you've put the
# tasker.css file that comes with Tasker (or your own customized style sheet).
style_url = '/styles/tasker.css'

# Shortcuts for the shortcut bar.  This variable must be set to a list
# (enclosed in []) of tuples (enclosed in parens) separated by commas.  Each
# tuple should look like:
#
#     (<name>, <limit>, <group>)
#
# where <name> is what will show up as the link (and should be enclosed in
# single quotes), <limit> is a number from 1 to 5 indicating not to show tasks
# with a priority lower than that or the number 0 indicating that all tasks
# should be shown, and <group> is either a task group to restrict oneself to
# or the value '' to show tasks from all groups.
#
# This is a bit complicated.  Hopefully the example will help.
shortcuts = [
    ('All', 0, ''),
    ('Urgent', 1, ''),
    ('High', 2, ''),
    ('Work', 0, 'Work')
]
